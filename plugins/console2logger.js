"use strict";
const path = require('path');
const fs = require('fs');
module.exports = function ({ types: t, template }) {
    return {
        visitor: {
            MemberExpression(path, state) {
                const replaceLog = calcReplaceLogStatement(path)
                if (replaceLog) {
                    path.replaceWith(t.memberExpression(t.identifier('logger'), t.identifier(replaceLog)));

                    //#region  import logger if its not imported
                    const ProgramPath = getProgramParent(path.scope);
                    let isLoggerImpored = false;
                    const body = ProgramPath.get("body");
                    for (let i = 0; i < body.length; i++) {
                        if (body[i].type === "ImportDeclaration") {
                            const localImport = body[i].get("specifiers")[0].get("local");
                            if (localImport.isIdentifier({ name: 'logger' })) isLoggerImpored = true
                        }

                    }
                    if (!isLoggerImpored) {
                        const loggerImportPath = getRelativePath(state.file.opts.filename);
                        const loggerImport = template(`import logger from "${loggerImportPath}";`, { sourceType: "module" });
                        const lastImport = body.filter(p => p.isImportDeclaration()).pop();
                        if (!lastImport) body[0].insertBefore(loggerImport())
                        else lastImport.insertAfter(loggerImport())
                    }
                    //#endregion
                }
            },
        }
    };
};


const getProgramParent = (scope) => {
    do {
        if (scope.path.isProgram()) {
            return scope.path;
        }
    } while (scope = scope.parent);
    throw new Error("We couldn't find a Function or Program...");
}

const getRelativePath = (filePath) => {
    const fileDir = path.dirname(filePath);
    // logger path 
    const loggerPath = path.resolve(__dirname, "../src/utils/logger.ts");
    if (!fs.existsSync(loggerPath)) throw new Error('incorrect logger path!');
    const loggerPathInfo = path.parse(loggerPath);
    const loggerPathWithoutExtension = path.resolve(loggerPathInfo.dir, loggerPathInfo.name)

    let result = path.relative(fileDir, loggerPathWithoutExtension);
    if (result) {
        result = result.replace(/\\/g, '/');
        if (!result.includes('..')) result = './' + result
    }
    return result;
}

const calcReplaceLogStatement = (path) => {
    const object = path.get("object");
    const property = path.get("property");
    const isConsole = object.isIdentifier({ name: 'console' });
    if (isConsole && property.isIdentifier({ name: 'log' })) return 'info';
    if (isConsole && property.isIdentifier({ name: 'info' })) return 'info';
    if (isConsole && property.isIdentifier({ name: 'error' })) return 'error';
    if (isConsole && property.isIdentifier({ name: 'warn' })) return 'warn';
    if (isConsole && property.isIdentifier({ name: 'debug' })) return 'debug';
    return;
}
