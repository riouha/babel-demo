// 0 npm init -y
// 1 install typescript
// 2 tsc --init => set tsconfig.json

// 3 install babel core,cli ::: npm i -D @babel/cli @babel/core
// 4 create .babelrc
// 5 install babel tsc plugin ::: npm i -D @babel/preset-typescript
// 6 use @babel/preset-typescript => "presets": ["@babel/preset-typescript"]
// 7 build with babel-cl ::: npx babel --extensions ".ts" src -d build OR IN pakage.json => "build": "babel --extensions '.ts' src -d build"

// 8 type checking
// 8.1 noEmit: true
// 8.2 tsc before build
